// All PIN and macro here

//BT macros
#define BT_name "CubeTest"

// buzzer macros
#define BUZZER 27
#define B_FREQ 500
#define B_PWM_CH 0
#define B_RES 2

// battery macros
#define VBAT 25
#define V_COEFF 264

// Motor Macros
#define BRAKE       15

#define DIR1        32
#define ENC1_1      34
#define ENC1_2      35
#define PWM1        33
#define PWM1_CH     1

#define DIR2        5
#define ENC2_1      2
#define ENC2_2      4
#define PWM2        18
#define PWM2_CH     2

#define DIR3        19
#define ENC3_1      39
#define ENC3_2      36
#define PWM3        23
#define PWM3_CH     3

#define TIMER_BIT   8
#define SPEED_STEP 32
#define MAX_SPEED_REL 0
#define MIN_SPEED_REL 8
#define MOT_FREQ   17000

//global vars

// motor
bool start = false;
bool turn_cw = true;
int pwmDutyRel = MIN_SPEED_REL;

