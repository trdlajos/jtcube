char handle_input(){
  // reads only first char on the line
  String inputString = "";
  char fChar = SBT.read();
  // Read until newline character or timeout, 
  while (SBT.available()) {
    if (SBT.read() == '\n') {
      break;  // Stop reading when newline is encountered
    }
    delay(10);  // Short delay to allow more data to be received
  }
  return fChar;
}

void buzz(){
  ledcWriteChannel(B_PWM_CH,1);
  delay(200);
  ledcWriteChannel(B_PWM_CH,0);
}

double battVoltage() {
  double voltage = (double) analogRead(VBAT) / V_COEFF;
  if (voltage > 8 && voltage <= 9.5) {
    buzz();
    buzz();
    buzz();
  }
  return voltage;
}

void do_command(char msg){
switch(msg){
  case 'r':
    start = true;
    break;
  case 's':
    start = false;
    break;
  case '1':
    update_motor(DIR1,turn_cw,start,1,pwmDutyRel);
    SBT.println("Motor1 updated!");
    break;
  case '2':
    update_motor(DIR2,turn_cw,start,2,pwmDutyRel);
    SBT.println("Motor2 updated!");
    break;
  case '3':
    update_motor(DIR3,turn_cw,start,3,pwmDutyRel);
    SBT.println("Motor3 updated!");
    break;
  case '-':
    pwmDutyRel += 1;
    if(pwmDutyRel>MIN_SPEED_REL){
      pwmDutyRel = MIN_SPEED_REL;
      SBT.println("Min reached!");
    }
    break;
  case '+':
    pwmDutyRel -= 1;
    if(pwmDutyRel<MAX_SPEED_REL){
      pwmDutyRel = MAX_SPEED_REL;
      SBT.println("Max reached!");
    }
    break;
  case 'c':
    turn_cw = !turn_cw;
    break;
  case 'h':
    print_help();
    break;
  case 'm':
    print_motor_status();
    break;
  case 'v':
    buzz();
  break;
  case 'b':
    SBT.print("Voltage: ");
    SBT.println(battVoltage());
  default: 
    SBT.println("Bad message");
  }
}

void update_motor(int pindir, bool dir, bool start, int pwm_ch, int pwm_duty_rel){
  // update motor pins
    digitalWrite(pindir, dir);
    digitalWrite(BRAKE, !start);
    ledcWriteChannel(pwm_ch,pwm_duty_rel*SPEED_STEP);
}

void print_help(){
  SBT.println("r/s ... run/stop the motor");
  SBT.println("c ... reverse - changes polarity");
  SBT.println("+/- ... increases/decreases speed");
  SBT.println("m ... status report of motor");
  SBT.println("b ... status of battery");
  SBT.println("v ... beep");
  SBT.println("1/2/3 ... updates corresponding motor");
}

void print_motor_status(){
  SBT.print("Enabled: ");
  SBT.println(start);
  SBT.print("Polarity: ");
  SBT.println(turn_cw ? "CW" : "CCW");
  SBT.print("SpeedRel: ");
  SBT.println(pwmDutyRel);
  SBT.print("SpeedPWM: ");
  SBT.println(SPEED_STEP*pwmDutyRel);
}