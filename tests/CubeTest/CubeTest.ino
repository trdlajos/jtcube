/*
CubeTest

Able to test all parts of Cube electronics - Motors, Buzzer, Battery voltage, Gyro/Accelerometer

functions: contains all functions used 
header: definitions of pins and macros

Motor notes:
3 controlling inputs(PWM, direction and brake) and 2 optical encoder outputs
Increase in duty cycle means decrease in velocity!

Commands:
h - help

r - starts motor
s - stops all operation
m - current motor settings
1 - updates mot1 to current motor settings
2 - updates mot2 to ...
3 - updates mot3 to ...
+ - increases velocity(decreases dutycycle)
- - decreases velocity()
c - change polarity(reverse)
b - battery reading
v - beep with buzzer

*/

#include "header.h"
#include "BluetoothSerial.h"


BluetoothSerial SBT;

bool a;
bool b;
bool c;

void setup() {

  pinMode(VBAT, INPUT);

  pinMode(BUZZER, OUTPUT);
  pinMode(BRAKE, OUTPUT);
  pinMode(DIR1, OUTPUT);
  pinMode(DIR2, OUTPUT);
  pinMode(DIR3, OUTPUT);
  pinMode(PWM1, OUTPUT);
  pinMode(PWM2, OUTPUT);
  pinMode(PWM3, OUTPUT);

  ledcAttachChannel(BUZZER,B_FREQ,B_RES,B_PWM_CH); 

  a = ledcAttachChannel(PWM1,MOT_FREQ,TIMER_BIT,PWM1_CH);  
  b = ledcAttachChannel(PWM2,MOT_FREQ,TIMER_BIT,PWM2_CH);  
  c = ledcAttachChannel(PWM3,MOT_FREQ,TIMER_BIT,PWM3_CH);   

  SBT.begin(BT_name);
}

void loop() {
  buzz();
  
  // listen to commands from controlling device
  if(SBT.available()){
    SBT.print(a);
    SBT.print(b);
    SBT.print(c);
    char msg = handle_input();
    do_command(msg);
  }

}
