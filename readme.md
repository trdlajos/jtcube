
# JTCube
*adapted project of REM-RC, his project [here](https://github.com/remrc/Self-Balancing-Cube)*

Code and schematics of self-balancing cube, build as part of my maker skills development endeavor.


* tests - holds a program for testing the cube <br>
* electro_desing - holds Fritzing files containing schematics(main has both graphic view and schematic view, second one has only graphic view) <br>
* esp32_cube_enc - holds original program adapted by me<br>
